# Test Project
This repository has the informations needed to develop the project.  
There is no time limit to submit the project, but we will prioritaze the projects in order that we receive then.  

## About the project
The project is based on e-commerce sites, and should be developed with react, and other technologies that you see fit.  
Please note that, your project needs to have at least the following features: add and remove product from the cart, change quantity, navigate through pages, and filter products by category.

## Pages
The project must have the following pages:
- Home - Product Listing with filters
- Product Detail Page
- Cart

You can add other pages if you think that is necessary.

## Technologies
The only technology that is mandatory is that it has to be developed with a Javascript Framework.  
You may add other libraries or plugins.

## Points that will be assessed
- Responsive Layout
- Code Structure / Organization
- Code / Assets Optimization
- React concepts
- Explanation of choosen technologies

## Plus
- State Management
- SSR
- PWA
- Accessibility
- Localization

## How to deliver
Project through private git repository, or compressed, keeping the history of the versioning system used, and how to run your project in development and production.  
Also you should explain why you choose the technologies, and how you implemented it.
